<?php
get_header(); ?>

    <section class="content col-xs-12">
        <div class="posts row col-sm-8">
            <?php query_posts($query_string . '&cat=-5'); ?>
            <?php if (have_posts()):
                while (have_posts()): the_post(); ?>
                    <?php setPostViews(get_the_ID()); ?>
                    <article class="post col-sm-6">
                        <div class="img-wrap">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                            </a>
                        </div>
                        <div class="post-content">
                            <h2>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <?php the_excerpt(); ?>
                            <div class="info row between-xs">
                                <div class="likes col-xs-4 start-xs">
                                    <a href="<?php the_permalink() ?>#likes">
                                        <span class="fa fa-heart"></span>
                                        <?php comments_number('0', '1', '%'); ?>
                                    </a>
                                </div>
                                <div class="date row end-xs">
                                    <span>By
                                        <?php the_author(); ?> /
                                    </span>
                                    <a href="<?php the_permalink() ?>#comments">
                                        <?php comments_number('0 comments', '1 comment', '% comments'); ?> /
                                    </a>
                                    <span><?php the_time( 'F j, Y ' ); ?></span>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php endwhile; ?>

            <?php else: ?>
                <p>No posts found</p>
            <?php endif; ?>

            <div class="pag-wrap col-sm-12 center-xs">
                <?php
                global $wp_query;

                $big = 999999999; // need an unlikely integer

                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'total' => $wp_query->max_num_pages,
                    'prev_text' => 'Prev',
                    'next_text' => 'Next'
                ) );
                ?>
            </div>

        </div>
        <?php get_sidebar(); ?>
    </section>

<?php get_footer(); ?>