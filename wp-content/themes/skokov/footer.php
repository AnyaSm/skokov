    <section class="about-info col-xs-12">
        <div class="container">
            <div class="about-us col-sm-4 col-xs-12">
                <div class="about">
                    <?php
                    query_posts('p=51');
                    while ( have_posts() ) : the_post(); ?>
                        <h2 class="tittle">
                            <?php the_title(); ?>
                        </h2>
                        <?php the_content(); ?>
                        <a class="button" href="<?php the_permalink(); ?>">Read more</a>
                    <?php endwhile; ?>
                </div>

                <div class="photo-stream col-xs-12">
                    <h2>Photo stream</h2>
                    <div class="img-wrap row">
                        <?php
                        $query = new WP_Query( array('post_type' => 'photo-reviews', 'posts_per_page' => 100 ) );
                        if ($query->have_posts()):?>
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                            <?php endwhile; ?>
                        <?php endif; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            <div class="latest-tweets col-sm-4 col-xs-12">
                <ul>
                    <?php if(!dynamic_sidebar('pre-footer-sidebar')) : ?>

                    <?php endif; ?>
                </ul>
                <div class="social">
                    <h2 class="col-xs-6">Social Connecting</h2>
                    <div class="footer-social col-xs-6">
                        <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>">
                            <span class="fa fa-facebook"></span>
                        </a>
                        <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>">
                            <span class="fa fa-twitter"></span>
                        </a>
                        <a class="google-plus" href="<?php echo get_theme_mod('social_links_google'); ?>">
                            <span class="fa fa-google-plus"></span>
                        </a>
                        <a class="youtube" href="<?php echo get_theme_mod('social_links_youtube'); ?>">
                            <span class="fa fa-youtube"></span>
                        </a>
                        <a class="linkedin" href="<?php echo get_theme_mod('social_links_instagram'); ?>">
                            <span class="fa fa-linkedin"></span>
                        </a>
                        <a class="dribbble" href="<?php echo get_theme_mod('social_links_dribbble'); ?>">
                            <span class="fa fa-dribbble"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="contact-info col-sm-4 col-xs-12">
                <ul>
                    <?php if(!dynamic_sidebar('info-sidebar')) : ?>

                    <?php endif; ?>
                </ul>
                <div class="follow-us">
                    <ul>
                        <?php if(!dynamic_sidebar('contact-sidebar')) : ?>

                        <?php endif; ?>
                    </ul>
                    <form class="blog-search">
                        <?php echo do_shortcode ('[contact-form-7 id="75" title="Untitled"]'); ?>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer col-xs-12">
            <div class="container">
                <ul class="col-sm-4 start-xs">
                    <?php if(!dynamic_sidebar('footer-sidebar')) : ?>

                    <?php endif; ?>
                </ul>
                <div id="navbar" class="collapse navbar-collapse">
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'main-nav',
                        'container'       => false,
                        'menu_class'      => 'nav navbar-nav main-menu-list row column-xs col-sm-8 end-sm'
                    ));
                    ?>
                </div><!--/.nav-collapse -->
            </div>
        </footer>

<?php wp_footer(); ?>

</body>
</html>