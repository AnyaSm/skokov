<aside class="sidebar col-sm-4 col-xs-12">

    <ul>
        <?php if(!dynamic_sidebar('sidebar')) : ?>
            <li>

            </li>
        <?php endif; ?>
    </ul>

    <div class="popular">
        <h3 class="widget-title">Popular Posts</h3>
        <ul>
            <?php
            $args = array( numberposts => 4, meta_key => post_views_count, orderby => meta_value_num, order => DESC );
            query_posts($args);
            while ( have_posts() ) : the_post();
            ?>
            <li class="col-xs-12">
                <div class="img-wrap col-xs-4">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                    </a>
                </div>
                <div class="cont col-xs-8">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                    <div class="likes end-xs">
                        <span><?php the_time( 'F j, Y ' ); ?>,</span>
                        <a href="<?php the_permalink() ?>#likes">
                            <span class="fa fa-heart"></span>
                            <?php comments_number('0', '1', '%'); ?>
                        </a>
                    </div>
                </div>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>

</aside>
