
<form name="search" action="<?php echo home_url( '/' ) ?>" method="get" class="search-form center-xs">
    <div class="form-wrap">
        <input type="text" placeholder="Search" autocomplete="off" value="<?php echo get_search_query() ?>"
               name="s" id="text" class="input">
        <button type="submit" class="for-search"></button>
    </div>
</form>