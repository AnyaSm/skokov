<?php

get_header(); ?>

    <section class="content col-xs-12">
        <div class="posts row col-sm-8">
            <?php query_posts($query_string . '&cat=-5'); ?>
            <?php if (have_posts()):
                while (have_posts()): the_post(); ?>
                    <?php setPostViews(get_the_ID()); ?>
                    <article class="post single-post col-xs-12">
                        <div class="img-wrap">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                            </a>
                        </div>
                        <div class="post-content">
                            <h2>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <div class="info row">
                                <div class="likes col-xs-4 start-xs">
                                    <?php echo do_shortcode ('[wp_ulike]'); ?>
                                    <a href="<?php the_permalink() ?>#likes">
                                        <span class="fa fa-heart"></span>
                                        <?php comments_number('0', '1', '%'); ?>
                                    </a>
                                </div>
                                <div class="date row col-xs-8 end-xs">
                                    <span>By
                                        <?php the_author(); ?> /
                                    </span>
                                    <a href="<?php the_permalink() ?>#comments">
                                        <?php comments_number('0 comments', '1 comment', '% comments'); ?> /
                                    </a>
                                    <span><?php the_time( 'F j, Y ' ); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php the_content(); ?>
                        <php get_related_posts_thumbnails(); ?>
                        <?php comments_template(); ?>
                    </article>
                <?php endwhile; ?>

            <?php else: ?>
                <p>No posts found</p>
            <?php endif; ?>
        </div>
        <?php get_sidebar(); ?>
    </section>

<?php get_footer(); ?>